﻿using Identity.Api.Data;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Patronworks.Core;
using Patronworks.Core.Extensions;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.Models;
using Patronworks.Core.Roles;
using Serilog;
using Serilog.Events;
using System.IO;

namespace Identity.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args)
                .MigrateDbContext<PersistedGrantDbContext>((_, __) => { })
                .MigrateDbContext<UserDbContext>((context, services) =>
                {
                    var env = services.GetService<IHostEnvironment>();
                    var logger = services.GetService<ILogger<ApplicationDbContextSeed>>();
                    var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
                    var roleManager = services.GetRequiredService<RoleManager<Role>>();
                    var rightService = services.GetRequiredService<IRightService>();
                    var roleDetailService = services.GetRequiredService<IRoleDetailService>();
                    new ApplicationDbContextSeed()
                        .SeedAsync(context, env, logger, userManager, roleManager, rightService, roleDetailService)
                        .Wait();
                })
                .MigrateDbContext<ConfigurationDbContext>((context, services) =>
                {
                    var configuration = services.GetService<IConfiguration>();

                    new ConfigurationDbContextSeed()
                        .SeedAsync(context, configuration)
                        .Wait();
                })

                .Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            Microsoft.AspNetCore.WebHost.CreateDefaultBuilder(args)
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                //.UseUrls(new[] { "http://0.0.0.0:7506" })
                //.ConfigureKestrel(serverOptions =>
                //{
                //    serverOptions.Listen(IPAddress.Loopback, 7506);
                //})
                .ConfigureAppConfiguration((builderContext, config) =>
                {
                    var builtConfig = config.Build();

                    var configurationBuilder = new ConfigurationBuilder();

                    //if (Convert.ToBoolean(builtConfig["UseVault"]))
                    //{
                    //    configurationBuilder.AddAzureKeyVault(
                    //        $"https://{builtConfig["Vault:Name"]}.vault.azure.net/",
                    //        builtConfig["Vault:ClientId"],
                    //        builtConfig["Vault:ClientSecret"]);
                    //}

                    configurationBuilder.AddEnvironmentVariables();

                    config.AddConfiguration(configurationBuilder.Build());
                })
                .ConfigureLogging((hostingContext, builder) =>
                {
                    builder.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    builder.AddFilter("Microsoft", LogLevel.Warning)
                        .AddFilter("System", LogLevel.Warning)
                        .AddConsole();
                    builder.AddDebug();
                })
                //.UseApplicationInsights()
                .UseSerilog((builderContext, config) =>
                {
                    config
                        .MinimumLevel.Information()
                        .MinimumLevel.Override("Microsoft.AspNetCore.Hosting.Internal.WebHost", LogEventLevel.Error)
                        .Enrich.FromLogContext()
                        .WriteTo.Console();
                })
                .Build();
    }
}

