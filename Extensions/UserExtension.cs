﻿using Identity.Api.Models;
using Patronworks.Core.Models;

namespace Identity.Api.Extensions
{
    public static class UserExtension
    {

        public static WebUser ToWebUser(this ApplicationUser applicationUser)
        {
            return new WebUser
            {
                FirstName = applicationUser.FirstName,
                LastName = applicationUser.LastName,
                Email = applicationUser.Email
            };
        }
    }
}
