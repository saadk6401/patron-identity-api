﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Patronworks.Core;
using Patronworks.Core.Attributes;
using Patronworks.Core.Interfaces.Service;
using Patronworks.Core.Models;
using Patronworks.Core.Roles;
using Patronworks.Core.Services;
using Sieve.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Api.Data
{
    public class ApplicationDbContextSeed
    {
        private readonly IPasswordHasher<ApplicationUser> _passwordHasher = new PatronworksPasswordHasher();

        public async Task SeedAsync(UserDbContext context, IHostEnvironment env, ILogger<ApplicationDbContextSeed> logger,
            UserManager<ApplicationUser> userManager, RoleManager<Role> roleManager, IRightService rightService, IRoleDetailService roleDetailService,
            int? retry = 0)
        {
            int retryForAvaiability = retry.Value;

            try
            {
                var contentRootPath = env.ContentRootPath;
                await UpdateRights(contentRootPath, rightService);
                //var webroot = env.WebRootPath;

                //if (!await roleManager.RoleExistsAsync("MobileUser"))
                //{
                //    var role = new UserIdentityRole { Name = "MobileUser" };
                //    await roleManager.CreateAsync(role);
                //}

                if (!await roleManager.RoleExistsAsync("Employee"))
                {
                    var role = new Role { Name = "Employee" };
                    await roleManager.CreateAsync(role);
                }

                if (!await roleManager.RoleExistsAsync("Admin"))
                {
                    var role = new Role { Name = "Admin" };
                    await roleManager.CreateAsync(role);
                }

                await UpdateRoleWithRights("Admin", roleManager, roleDetailService);
                await UpdateRoleWithRights("Employee", roleManager, roleDetailService, true);

                if (!context.Users.Any())
                {
                    var defaultUser = GetDefaultUser();
                    context.Users.Add(defaultUser);
                    await context.SaveChangesAsync();
                    await userManager.AddToRoleAsync(defaultUser, "Admin");
                }

            }
            catch (Exception ex)
            {
                if (retryForAvaiability < 10)
                {
                    retryForAvaiability++;

                    logger.LogError(ex.Message, $"There is an error migrating data for ApplicationDbContext");

                    await SeedAsync(context, env, logger, userManager, roleManager, rightService, roleDetailService, retryForAvaiability);
                }
            }
        }


        private ApplicationUser GetDefaultUser()
        {
            var user =
            new ApplicationUser()
            {
                Id = Guid.Parse("d9db7803-e322-4ea4-9295-0177a253afa8"),
                Email = "admin@patronworks.net",
                LastName = "Patronworks",
                FirstName = "Admin",
                UserName = "admin",
                NormalizedEmail = "ADMIN@PATRONWORKS.NET",
                NormalizedUserName = "ADMIN",
                EmailConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D"),
            };

            user.PasswordHash = _passwordHasher.HashPassword(user, "1q2w3e4r$");
            return user;
        }
        private async Task UpdateRoleWithRights(string roleName, RoleManager<Role> roleManager, IRoleDetailService roleDetailService, bool onlyDefaults = false)
        {
            var role = await roleManager.FindByNameAsync(roleName);
            if (role != null)
            {
                await roleDetailService.UpdateRoleWithNewRights(role.Id, onlyDefaults);
            }
        }
        private async Task UpdateRights(string contentRootPath, IRightService rightService)
        {
            var newRights = new List<Right>();
            var rights = (await rightService.LoadAll(new SieveModel())).Data;
            var rightsFromJson = GetJsonRights(contentRootPath);
            foreach (var jsonRight in rightsFromJson)
            {
                var right = rights.FirstOrDefault(f => f.RightKey == jsonRight.RightKey);
                if (right == null)
                {
                    await rightService.Insert(jsonRight);
                }
                else
                {
                    await rightService.UpdateRights(right, jsonRight);
                }
            }
        }
        private List<Right> GetJsonRights(string contentRootPath)
        {
            string languagesJson = Path.Combine(contentRootPath, "Setup", "rights");
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.ContractResolver = new IgnoreJsonAttributesResolver();
            settings.Formatting = Formatting.Indented;
            var rights = JsonConvert.DeserializeObject<List<Right>>(File.ReadAllText(languagesJson), settings);
            return rights;
        }
    }
}
