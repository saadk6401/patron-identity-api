﻿using Identity.Api.Configuration;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Entities;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Api.Data
{
    public class ConfigurationDbContextSeed
    {
        public async Task SeedAsync(ConfigurationDbContext context, IConfiguration configuration)
        {

            //callbacks urls from config:
            var clientUrls = new Dictionary<string, string>();

            clientUrls.Add("Web", configuration.GetValue<string>("WebClient"));
            clientUrls.Add("PatronworksApi", configuration.GetValue<string>("PatronworksApiClient"));
            clientUrls.Add("UserApi", configuration.GetValue<string>("UserApiClient"));
            clientUrls.Add("WebAgg", configuration.GetValue<string>("WebAggClient"));
            clientUrls.Add("MobileAgg", configuration.GetValue<string>("MobileAggClient"));


            if (!context.Clients.Any())
            {
                foreach (var client in Config.GetClients(clientUrls))
                {
                    context.Clients.Add(client.ToEntity());
                }
                await context.SaveChangesAsync();
            }
            // Checking always for old redirects to fix existing deployments
            // to use new swagger-ui redirect uri as of v3.0.0
            // There should be no problem for new ones
            // ref: https://github.com/dotnet-architecture/eShopOnContainers/issues/586
            else
            {
                //Check if new clients
                var saveData = false;
                var clients = Config.GetClients(clientUrls);
                foreach (var client in clients)
                {
                    var entityClient = await context.Clients.Include(c => c.RedirectUris).FirstOrDefaultAsync(f => f.ClientId == client.ClientId);
                    if (entityClient == null)
                    {
                        context.Clients.Add(client.ToEntity());
                        saveData = true;
                    }
                    else
                    {
                        var newRedirects = client.RedirectUris.Where(f => !entityClient.RedirectUris.Select(c => c.RedirectUri).Contains(f)).ToList();
                        if (newRedirects.Count() > 0)
                        {
                            entityClient.RedirectUris.AddRange(newRedirects.Select(c => new ClientRedirectUri { ClientId = entityClient.Id, RedirectUri = c }));
                            context.Update(entityClient);
                            saveData = true;
                        }
                    }

                }
                if (saveData)
                {
                    await context.SaveChangesAsync();
                }
                //var x =clients.SelectMany(f => f.RedirectUris).ToList();
                List<ClientRedirectUri> oldRedirects = (await context.Clients.Include(c => c.RedirectUris).ToListAsync())
                .SelectMany(c => c.RedirectUris)
                .Where(ru => ru.RedirectUri.EndsWith("/o2c.html"))
                .ToList();

                if (oldRedirects.Any())
                {
                    foreach (var ru in oldRedirects)
                    {
                        ru.RedirectUri = ru.RedirectUri.Replace("/o2c.html", "/oauth2-redirect.html");
                        context.Update(ru.Client);
                    }
                    await context.SaveChangesAsync();
                }
            }

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in Config.GetResources())
                {
                    context.IdentityResources.Add(resource.ToEntity());
                }
                await context.SaveChangesAsync();
            }
            else
            {
                var saveData = false;
                foreach (var resource in Config.GetResources())
                {
                    var entityResource = context.IdentityResources.FirstOrDefault(f => f.Name == resource.Name);
                    if (entityResource == null)
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                        saveData = true;
                    }
                }
                if (saveData)
                {
                    await context.SaveChangesAsync();
                }
            }


            if (!context.ApiScopes.Any())
            {
                foreach (var api in Config.GetApiScopes())
                {
                    context.ApiScopes.Add(api.ToEntity());
                }

                await context.SaveChangesAsync();
            }
            else
            {
                var saveData = false;
                foreach (var api in Config.GetApiScopes())
                {
                    var entityApi = context.ApiScopes.FirstOrDefault(f => f.Name == api.Name);
                    if (entityApi == null)
                    {
                        context.ApiScopes.Add(api.ToEntity());
                        saveData = true;
                    }
                }
                if (saveData)
                {
                    await context.SaveChangesAsync();
                }
            }

            if (!context.ApiResources.Any())
            {
                foreach (var api in Config.GetApis())
                {
                    context.ApiResources.Add(api.ToEntity());
                }

                await context.SaveChangesAsync();
            }
            else
            {
                var saveData = false;
                foreach (var api in Config.GetApis())
                {
                    var entityApi = context.ApiResources.FirstOrDefault(f => f.Name == api.Name);
                    if (entityApi == null)
                    {
                        context.ApiResources.Add(api.ToEntity());
                        saveData = true;
                    }
                }
                if (saveData)
                {
                    await context.SaveChangesAsync();
                }
            }
        }
    }
}
