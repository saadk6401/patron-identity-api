﻿using Autofac;
using Autofac.Extensions.DependencyInjection;
using HealthChecks.UI.Client;
using Identity.Api.Services;
using Identity.Api.Translate;
using IdentityServer4.Services;
using IdentityServer4.Validation;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Patronworks.Core;
using Patronworks.Core.Extensions;
using Patronworks.Core.Models;
using Patronworks.Core.Roles;
using System;
using System.Reflection;

namespace Identity.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private string ConnectionString
        {
            get
            {
                var conn = $"Server={Configuration["POSTGRES_HOSTNAME"]};Port={Configuration["POSTGRES_PORT"]};Database={Configuration["IDENTITY_DATABASE"]};User Id={Configuration["POSTGRES_USERNAME"]};Password={Configuration["POSTGRES_PASSWORD"]};";
                return conn;
            }
        }
        private string LocazationConnectionString
        {
            get
            {
                var conn = $"Server={Configuration["POSTGRES_HOSTNAME"]};Port={Configuration["POSTGRES_PORT"]};Database={Configuration["LOCALIZATION_DATABASE"]};User Id={Configuration["POSTGRES_USERNAME"]};Password={Configuration["POSTGRES_PASSWORD"]};";
                return conn;
            }
        }

        private string LogConnectionString()
        {
            var conn = $"Server={Configuration["POSTGRES_HOSTNAME"]};Port={Configuration["POSTGRES_PORT"]};Database={Configuration["LOG_DATABASE"] ?? "Patronworks.Log"};User Id={Configuration["POSTGRES_USERNAME"]};Password={Configuration["POSTGRES_PASSWORD"]};";
            return conn;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            //RegisterAppInsights(services);
            var connectionString = this.ConnectionString;
            // Add framework services.
            services.AddDbContext<UserDbContext>(options =>
            {
                options.UseNpgsql(connectionString,
                       npgsqlOptionsAction: sqlOptions =>
                       {
                           sqlOptions.MigrationsAssembly(migrationsAssembly);
                           //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                           sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                           sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                       });
                options.EnableSensitiveDataLogging();
            }
             );

            services.AddDbContext<LocalizationDBContext>(options =>
            {
                options.UseNpgsql(LocazationConnectionString,
                       npgsqlOptionsAction: sqlOptions =>
                       {
                           sqlOptions.MigrationsAssembly(migrationsAssembly);
                           //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                           sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                           sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                       });
                //options.EnableSensitiveDataLogging();
            }
            );

            services.AddDbContext<LogDbContext>(options =>
            {
                options.UseNpgsql(LogConnectionString(),
                     npgsqlOptionsAction: sqlOptions =>
                     {
                         sqlOptions.MigrationsAssembly(migrationsAssembly);
                         //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                         sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                         sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                     }).EnableSensitiveDataLogging(true);
            });

            services.AddIdentity<ApplicationUser, Role>(cfg =>
            {
                // cfg.SignIn.RequireConfirmedEmail = true;
            })
                .AddErrorDescriber<MultilanguageIdentityErrorDescriber>()
                .AddEntityFrameworkStores<UserDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);


            services.AddHealthChecks()
                .AddCheck("self", () => HealthCheckResult.Healthy())
                .AddNpgSql(connectionString,
                    name: "IdentityDB-check",
                    tags: new string[] { "IdentityDB" });

            // ConfigureRedis(services, Configuration);
            services.AddCoreServices(Configuration);
            ConfigureApiServices(services, Configuration);

            //services.ConfigureAuthService(Configuration, "user");

            // Adds IdentityServer
            services.AddIdentityServer(x =>
            {
                x.IssuerUri = "null";
                x.Authentication.CookieLifetime = TimeSpan.FromHours(2);
            })
            .AddSigningCredential(Certificate.Certificate.Get())
            .AddAspNetIdentity<ApplicationUser>()
            .AddResourceOwnerValidator<ResourceOwnerPasswordValidator<ApplicationUser>>()
            //TODO
            //.AddExtensionGrantValidator<AnonymousGrandValidator>()
            .AddConfigurationStore(options =>
            {
                options.ConfigureDbContext = builder => builder.UseNpgsql(connectionString,
                                npgsqlOptionsAction: sqlOptions =>
                                {
                                    sqlOptions.MigrationsAssembly(migrationsAssembly);
                                    //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                                    sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                                }).EnableSensitiveDataLogging();
            })
            .AddOperationalStore(options =>
            {
                options.ConfigureDbContext = builder => builder.UseNpgsql(connectionString,
                               npgsqlOptionsAction: sqlOptions =>
                               {
                                   sqlOptions.MigrationsAssembly(migrationsAssembly);
                                   //Configuring Connection Resiliency: https://docs.microsoft.com/en-us/ef/core/miscellaneous/connection-resiliency 
                                   sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);
                                   sqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery);
                               }).EnableSensitiveDataLogging();
            })
            .Services.AddTransient<IProfileService, ProfileService>();

            services.Configure<IdentityOptions>(opt =>
            {
                opt.Password.RequiredLength = 6;
                opt.Password.RequireDigit = true;
                opt.Password.RequireLowercase = false;
                opt.Password.RequireUppercase = false;
                opt.Password.RequireNonAlphanumeric = false;
                opt.User.RequireUniqueEmail = true;
                opt.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+$";
                opt.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                opt.Lockout.MaxFailedAccessAttempts = 5;
                opt.Lockout.AllowedForNewUsers = true;
            });

            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme);
            //.AddCookie("Cookies", options =>
            //{
            //    options.Cookie.SameSite = SameSiteMode.None;
            //    options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
            //});
            //services.AddDatabaseDeveloperPageExceptionFilter();
            services.ConfigureNonBreakingSameSiteCookies();

            var container = new ContainerBuilder();
            container.RegisterCache();
            container.Populate(services);
            return new AutofacServiceProvider(container.Build());
        }
        private void ConfigureApiServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<ILoginService<ApplicationUser>, EFLoginService>();
            services.AddTransient<IRedirectService, RedirectService>();
            services.AddTransient<IResourceOwnerPasswordValidator, ResourceOwnerPasswordValidator<ApplicationUser>>();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostEnvironment env, ILoggerFactory loggerFactory)
        {
            //loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            //loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            var pathBase = Configuration["PATH_BASE"];
            if (!string.IsNullOrEmpty(pathBase))
            {
                loggerFactory.CreateLogger("init").LogDebug($"Using PATH BASE '{pathBase}'");
                app.UsePathBase(pathBase);
            }

            app.UseHealthChecks("/hc", new HealthCheckOptions()
            {
                Predicate = _ => true,
                ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
            });

            app.UseHealthChecks("/liveness", new HealthCheckOptions
            {
                Predicate = r => r.Name.Contains("self")
            });

            app.UseCookiePolicy();

            app.Use(async (context, next) =>
            {
                context.Response.Headers.Add(
                    "Content-Security-Policy",
                    "script-src 'self' 'sha256-Bjw0tbM5vAaD9soptLO7Hs66rxxskisVs/9rvAiSEl0=' 'sha256-jz6O/Aum5WQLQHOi99rEjkeC7pe2OAqg7IGr+lQ/Ffk=' https://*.aspnetcdn.com;" +
                    "style-src 'self' 'unsafe-inline' https://fonts.googleapis.com; " +
                    "font-src 'self' https://fonts.gstatic.com; " +
                    "img-src 'self'");

                await next();
            });

            app.UseStaticFiles();


            // Make work identity server redirections in Edge and lastest versions of browers. WARN: Not valid in a production environment.
            //app.Use(async (context, next) =>
            //{
            //    context.Response.Headers.Add("Content-Security-Policy", "script-src 'unsafe-inline'");
            //    await next();
            //});

            app.UseForwardedHeaders();
            // Adds IdentityServer
            app.UseIdentityServer();

            app.UseAuthentication();

            //app.UseHttpsRedirection();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


        }


    }
}
