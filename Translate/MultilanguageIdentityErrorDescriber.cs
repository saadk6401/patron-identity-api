﻿using Microsoft.AspNetCore.Identity;

namespace Identity.Api.Translate
{
    public class MultilanguageIdentityErrorDescriber : IdentityErrorDescriber
    {

        public override IdentityError DuplicateUserName(string userName)
        {
            return new IdentityError()
            {
                Code = nameof(DuplicateUserName),
                Description = "The email address is already in use.Please choose a different one."
            };
        }
        public override IdentityError DuplicateEmail(string email)
        {
            return new IdentityError()
            {
                Code = nameof(DuplicateEmail),
                Description = "The email address is already in use.Please choose a different one."
            };
        }
        public override IdentityError InvalidEmail(string email)
        {
            return new IdentityError()
            {
                Code = nameof(InvalidEmail),
                Description = "The email address is not valid. Please enter a valid one. "
            };
        }
        public override IdentityError InvalidUserName(string userName)
        {
            return new IdentityError()
            {
                Code = nameof(InvalidUserName),
                Description = "The email address is not valid. Please enter a valid one. "
            };
        }
    }
}
