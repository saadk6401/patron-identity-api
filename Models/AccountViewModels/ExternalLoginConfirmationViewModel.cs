﻿using System.ComponentModel.DataAnnotations;

namespace Identity.Api.Models.AccountViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
