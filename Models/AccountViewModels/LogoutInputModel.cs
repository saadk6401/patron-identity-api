﻿namespace Identity.Api.Models.AccountViewModels
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
