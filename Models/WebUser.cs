﻿namespace Identity.Api.Models
{
    public class WebUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Organisation { get; set; }
        public string Email { get; set; }
    }
}
