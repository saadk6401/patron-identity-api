﻿namespace Identity.Api
{
    public class URLOptions
    {
        public string StudioUrl { get; set; }
        public string WebsurveyURL { get; set; }
    }
}
