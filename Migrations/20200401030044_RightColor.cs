﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Api.Migrations
{
    public partial class RightColor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "Right",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color",
                table: "Right");
        }
    }
}
