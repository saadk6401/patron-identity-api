﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Api.Migrations.ConfigurationDb
{
    public partial class UpdateApiScopes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            var updateApiScopes = @"Update  ""ApiScopes"" Set ""Enabled"" =true";
            migrationBuilder.Sql(updateApiScopes);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
