﻿using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace Identity.Api.Configuration
{
    public class Config
    {
        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("patronworks", "patronworks Service"),
                new ApiScope("webagg", "Web Aggregator"),
                new ApiScope("user", "User Service"),
            };
        }

        // ApiResources define the apis in your system
        public static IEnumerable<ApiResource> GetApis()
        {
            return new List<ApiResource>
            {
                new ApiResource("patronworks", "patronworks Service"),
                new ApiResource("webagg", "Web Aggregator"),
                new ApiResource("user", "User Service"),
            };
        }

        // Identity resources are data like user ID, name, or email address of a user
        // see: http://docs.identityserver.io/en/release/configuration/resources.html
        public static IEnumerable<IdentityResource> GetResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile()
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(Dictionary<string, string> clientsUrl)
        {
            var client = new Client
            {
                ClientId = "patronworks_mobile",
                ClientName = "patronworks mobile",
                AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                RequireConsent = false,
                AllowAccessTokensViaBrowser = true,
                AccessTokenLifetime = 2592000,
                AuthorizationCodeLifetime = 2592000,
                IdentityTokenLifetime = 2592000,
                ClientSecrets =
                    {
                        new Secret("YfdzV4sSlh7H0DBffefB".Sha256())
                    },
                AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OfflineAccess,
                        "patronworks",
                        "user"
                    }
            };
            return new List<Client>
            {
               client,
                // JavaScript Client
                new Client
                {
                    ClientId = "lampix_device",
                    ClientName = "lampix device",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenLifetime = 2592000,
                    AuthorizationCodeLifetime = 2592000,
                    IdentityTokenLifetime = 2592000,
                    ClientSecrets =
                        {
                            new Secret("YIOSKa8SbIw2DrlEHj4g".Sha256())
                        },
                    AllowedScopes = new List<string>
                        {
                            IdentityServerConstants.StandardScopes.OpenId,
                            IdentityServerConstants.StandardScopes.Profile,
                            IdentityServerConstants.StandardScopes.OfflineAccess,
                            "patronworks",
                            "user"
                        }
                },
                new Client
                {
                    ClientId = "patronworks_web",
                    ClientName = "patronworks Web OpenId Client",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    AccessTokenLifetime = 2592000,
                    AuthorizationCodeLifetime = 2592000,
                    IdentityTokenLifetime = 2592000,
                    RedirectUris = { $"{clientsUrl["Web"]}", "http://localhost:18010" },
                    RequireConsent = false,
                    PostLogoutRedirectUris = { $"{clientsUrl["Web"]}", "http://localhost:18010" },
                    AllowedCorsOrigins =     { $"{clientsUrl["Web"]}", "http://localhost:18010" },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "patronworks",
                        "user"
                    },
                },
                new Client
                {
                    ClientId = "patronworkswaggerui",
                    ClientName = "patronworks Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent=false,
                    AccessTokenLifetime = 2592000,
                    AuthorizationCodeLifetime = 2592000,
                    IdentityTokenLifetime = 2592000,
                    RedirectUris = { $"{clientsUrl["PatronworksApi"]}/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { $"{clientsUrl["PatronworksApi"]}/swagger/" },

                    AllowedScopes =
                    {
                        "patronworks"
                    }
                },
                    new Client
                {
                    ClientId = "userswaggerui",
                    ClientName = "User Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent=false,
                    AccessTokenLifetime = 2592000,
                    AuthorizationCodeLifetime = 2592000,
                    IdentityTokenLifetime = 2592000,
                    RedirectUris = { $"{clientsUrl["UserApi"]}/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { $"{clientsUrl["UserApi"]}/swagger/" },

                    AllowedScopes =
                    {
                        "user"
                    }
                },
                new Client
                {
                    ClientId = "webaggswaggerui",
                    ClientName = "Web Aggregattor Swagger UI",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent=false,
                    AccessTokenLifetime = 2592000,
                    AuthorizationCodeLifetime = 2592000,
                    IdentityTokenLifetime = 2592000,
                    RedirectUris = { $"{clientsUrl["WebAgg"]}/swagger/oauth2-redirect.html" },
                    PostLogoutRedirectUris = { $"{clientsUrl["WebAgg"]}/swagger/" },

                    AllowedScopes =
                    {
                        "webagg"
                    }
                }
            };
        }
    }
}